# CI/CD
### Travail demandé
Vous utiliserez Gitlab-CI pour automatiser toutes les taches de déploiement mais également les tests du bon fonctionnement des applications.
Nous vous proposons un découpage en trois stages : 
•	Build, 
•	Test 
•	Deploy.
La partie « Test » vérifiera que vos projets GO et BLOG sont fonctionnels. 
Le déploiement se fera sur 3 containers en load Balancing avec l’outils de votre choix. (Pour les quadrinômes, il se fera sur 3 containers en load Balancing et sur 3 autres containers en Failover )

