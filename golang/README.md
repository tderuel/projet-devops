# Golang
### Travail demandé

Vous devez développer un plugin Telegraf qui enregistre des valeurs données par powertop (https://wiki.archlinux.org/index.php/powertop) sur l’infrastructure docker hébergeant le blog (cf partie Docker) dans une DB InfluxDB. Afin d’y suivre les graphes de consommation électrique des machines.(https://www.influxdata.com/training/write-your-own-telegraf-plugin/)

Votre projet Golang sera testé et déployé en image Docker via GITLAB CI

