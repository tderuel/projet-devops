#!/bin/bash
service mysql start
mysql -e "CREATE DATABASE IF NOT EXISTS database;"
mysql -e "CREATE USER 'toto'@'localhost' IDENTIFIED by 'titi';"
mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'toto'@'localhost' IDENTIFIED BY 'titi';"
mysql -e "FLUSH PRIVILEGES;"
tail -F keep running
exec "$@"
