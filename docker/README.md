# Docker
## Travail demandé
En plus du projet Golang, il vous sera demandés d’installer une infrastructure Web hébergeant un Blog (libre choix de la solution, mais obligation d’une BDD déportée). Tous les services devront être containérisés dans leur propre environnement en respectant :
•	Un LoadBalancing est obligatoire 
•	Vous devez créer vos propres images Docker via dockerfile (toutes récupérations d’image existante est éliminatoire) 
•	Les images seront rajoutées à un DOCKER REGISTRY PRIVÉE. 
•	Le Docker registry devra également gérer l’authentification et les contrôles d’accès des utilisateurs via un « authentification server ». Vous aurez le choix de référencer vos utilisateurs via des ACL, ou MongoDB, ou LDAP, ou un fichier YAML (partie supplémentaire pour les quadrinômes)

Votre projet Blog sera testé et déployé en image Docker via GITLAB CI

